#!/usr/bin/env python
#
# Creation:
#   February 2014
#
# Author:
#   Miguel Filipe Silva Ribeiro [IST] (original)
#   Guillem Vallicrosa [UDG] (modifications)
#
# Description:
#   This code is intended for reading vehicle position from odometry and send it
#   to the simulated modems provided by CMRE over a VPN.

# ROS basics
import roslib 
roslib.load_manifest('fake_range_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry

# Internet communications
import socket

#===============================================================================
class Odom2SimModem(object):
    '''
    Class to hold the read of Odometry messages and send position to the
    simulated modem.
    '''
    
    #===========================================================================
    def __init__(self):
        '''
        Initializing the necessary variables.
        '''
        # Parameters
        ip = rospy.get_param('~ip', '10.42.23.1')
        port = rospy.get_param('~port', 11000)
        self.address = (ip, port)
        
        # Socket connection
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.sock.connect(self.address)
        rospy.loginfo("\nOdom2SimModem:\n\tConnected to %s:%d", ip, port)
        
        # Time
        self.time = rospy.Time.now()
        
        # Subscriber
        self.sub_odom = rospy.Subscriber('/pose_ekf_slam/odometry', 
                                         Odometry, self.odom_callback)
        
    #===========================================================================
    def odom_callback(self, msg):
        '''
        Reads Odometry and sends position.
        '''
        # Check time
        if (msg.header.stamp - self.time).to_sec() < 1.0:
            return
        
        # Update time
        self.time = msg.header.stamp
        
        # Prepare message
        data = "%.1f %.1f %.1f\n" % (msg.pose.pose.position.x,
                                     msg.pose.pose.position.y,
                                     msg.pose.pose.position.z)
        # Send
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect(self.address)
            self.sock.sendall(data)
            self.sock.close()
        except Exception, e:
            rospy.logwarn('%s : %s : error sending position to modem', rospy.get_name(), e)
            self.sock.close()

#===============================================================================
if __name__ == '__main__':
    
    # Start sending
    rospy.init_node('Odom2SimModem')
    node = Odom2SimModem()
    rospy.spin()
