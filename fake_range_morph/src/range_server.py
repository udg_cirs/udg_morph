#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for receiving uncompleted AModem messages from other 
#   vehicles and finish them by adding the range as tof.

# ROS basic
import roslib
roslib.load_manifest('fake_range_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry
#from morph_msgs.msg import AModem
from acomms.msg import AddressDurationPair

# UDP connection
import socket
import struct
import time
import math

class RangeServer(object):
    '''
    Class that reads UDP and creates a AModem message to simulate range.
    the UWSim.
    '''
	
    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        odom_name = rospy.get_param('range_server/odom_name', '/pose_ekf_slam/odometry')
        pub_name = rospy.get_param('range_client/pub_name', '/driver/range')

        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(0.5)
        self.sock.bind(("", 9605))

        # Publisher
        self.pub = rospy.Publisher(pub_name, AddressDurationPair)

        # Subscriber
        self.subOdom = rospy.Subscriber(odom_name, Odometry, self.updateOdom)

        # Flag
        self.initOdom = False

        # Data
        self.data = [0, 0, 0] # x, y, z

        # Info
        print 'Initialization:'
        print '   odom:', odom_name
        print '    pub:', pub_name
		
    def iterate(self):
        '''
        Listens to each UDP port and sends data to simulator.
        '''
        if self.initOdom:
            try:
                # Read until timeout
                msg = self.readSocket()
                
                # Publish
                self.pub.publish(msg)
            except Exception, e:
                #print e
                pass
        time.sleep(0.1)
		
    def updateOdom(self, msg):
        '''
        Reads Odometry from message and saves it.
        '''
        # Save data
        self.data[0] = msg.pose.pose.position.x
        self.data[1] = msg.pose.pose.position.y
        self.data[2] = msg.pose.pose.position.z
        self.initOdom = True
		
    def readSocket(self):
        '''
        Reads Odometry data from specified socket.
        '''
        # Prepare data
        fmt = '> 3d b s'
        msg = AddressDurationPair() #AModem()
        packet = self.sock.recv(struct.calcsize(fmt))

        # Read
        data = struct.unpack(fmt, packet)
        #msg.header.stamp = rospy.Time.now()
        msg.address = data[3]
        #msg.ID = data[3] 
        #msg.data = data[4]

        # Time of flight
        x1, y1, z1 = data[:3]
        x2, y2, z2 = self.data
        ran = math.sqrt( (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 )
        #msg.time_of_flight = int( ran/1500.*1e6 )
        # in rof.py --> range_msg.time_delay/2000000.0)*self.v_sound
        msg.time_delay = int( ran/1500.*2e6 )

        print 'fake range:', ran, '  from ID:', msg.address

        return msg		

if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('range_server')
    
    # Start node
    server = RangeServer()
    while not rospy.is_shutdown():
        server.iterate()
