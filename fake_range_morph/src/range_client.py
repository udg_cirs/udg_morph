#!/usr/bin/env python
#
# Creation:
#   July 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading vehicle position from odometry and beta 
#   from follow_section and send it through UDP to the vehicle running a ROF 
#   mission to complete the message.

# ROS basic
import roslib
roslib.load_manifest('fake_range_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry
from std_msgs.msg import Int8
#from morph_msgs.msg import AModem

# UDP connection
import socket
import struct

class RangeClient(object):
    '''
    Class that sends vehicle pose, ID and beta through UDP to vehicles running
    ROF for them to complete the AModem message.
    '''

    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        odom_name = rospy.get_param('range_client/odom_name', '/pose_ekf_slam/odometry')
        beta_name = rospy.get_param('range_client/beta_name', '/beta_to_transmit')
        hosts = rospy.get_param('range_client/hosts', list())
        self.addresses = [(host, 9605) for host in hosts]

        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Subscriber
        self.subOdom = rospy.Subscriber(odom_name, Odometry, self.updateOdom)
        #self.subBeta = rospy.Subscriber(beta_name, AModem, self.updateBeta)
        self.subID = rospy.Subscriber('/ID', Int8, self.updateID)

        # Flags
        self.initOdom = False
        #self.initBeta = False
        self.initID = False

        # Data
        self.data = [0, 0, 0, 0, 0] # x, y, z, ID, beta

        # Info
        print 'Initialization:'
        print '   hosts:', hosts
        print '    odom:', odom_name
        print '    beta:', beta_name
		
    def updateOdom(self, msg):
        '''
        Reads Odometry from message and saves it.
        '''
        # Save data
        self.data[0] = msg.pose.pose.position.x
        self.data[1] = msg.pose.pose.position.y
        self.data[2] = msg.pose.pose.position.z
        self.initOdom = True

    #def updateBeta(self, msg):
        #'''
        #Reads AModem from message and saves it.
        #'''
        # Save data
        #self.data[3] = msg.ID
        #self.data[4] = msg.data
        #self.initBeta = True
        
    def updateID(self, msg):
        '''
        Knows the ID of the vehicle.
        '''
        self.data[3] = msg.data
        self.data[4] = '0' # beta, not used
        self.initID = True
		
    def iterate(self):
        '''
        Sends saved data through UDP.
        '''
        if self.initOdom and self.initID:#self.initBeta:
            fmt = '> 3d b s'
            data = struct.pack(fmt, *self.data)

            # Send to all ROF vehicles
            for address in self.addresses:
                # Send data
                count = 0
                while count < len(data):
                    count += self.sock.sendto(data, address)
                print 'send: ', count, address
		

if __name__ == '__main__':

	# Initialize ROS node
	rospy.init_node('range_client')

	# Start node
	client = RangeClient()
	r = rospy.Rate(1./6.)
	while not rospy.is_shutdown():
		client.iterate()
		r.sleep()
