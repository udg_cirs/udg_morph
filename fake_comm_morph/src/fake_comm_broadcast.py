#!/usr/bin/env python
#
# Creation:
#   July 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended as a temporary replacement of communication node for 
#   simulations and first tests on vehicle formations.

# ROS basic
import roslib
roslib.load_manifest('fake_comm_morph')
import rospy

# MORPH messsages
from morph_msgs.msg import Pose_in_Section, Poses_in_Section

# UDP connection
import socket
import struct

# Toggle debug
DEBUG = True
MODEM_PERIOD = 3.0

#=======================================================================
class FakeComm(object):
    '''
    Class to handle sending Pose_in_Section messages to all other vehicles, and
    construct Poses_in_Section messages from what it received from the others.
    '''
    
    #===================================================================
    def __init__(self):
        '''
        Loads the parameters needed and starts the publishers and subscribers.
        '''
        # Parameters
        self.address = ('<broadcast>', 9604)
        self.ID = None
        
        # Socket to send
        self.sock_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_send.bind(('', 0))
        self.sock_send.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        # Socket to read
        self.sock_recv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_recv.settimeout(0.05)
        self.sock_recv.bind(('', 9604))
        
        # Publisher
        self.pub = rospy.Publisher('pose_in_section_other_vehicles', Poses_in_Section)
        self.secPoses = []
        self.msg = Poses_in_Section()
        
        # Subscriber
        self.sub = rospy.Subscriber('/pose_in_section', Pose_in_Section, self.poseSecCallback)
        
        # Time
        self.time = rospy.Time.now()
    
    #===================================================================
    def iterate(self):
        '''
        Receives information from all vehicles and publishes accordingly.
        '''
        # Need to know own ID
        if self.ID is None:
            return

        ## RECEIVE
        try:
            # Receive and deserialize message
            fmt = "< I 2i 20s B 2f b"
            data = self.sock_recv.recv(struct.calcsize(fmt))
            msg = self.deserialize(data)
            
            # Check if its myself
            if msg.ID == self.ID:
                return
            
            # Search and add msg
            found = False
            for i in range(len(self.secPoses)):
                if msg.ID == self.secPoses[i].ID:
                    if DEBUG: print '(update)',
                    self.secPoses[i] = msg
                    found = True
            if not found:
                if DEBUG: print '(new)',
                self.secPoses.append(msg)
                
            # Debug info
            if DEBUG:
                print 'ID: %d (%s) %d' % (msg.ID, ('new', 'update')[found], \
                                           msg.header.stamp.to_sec()) 
                
            # Publish all Poses (not all times)
            if msg.ID == self.secPoses[0].ID:
                self.msg.header.stamp = rospy.Time.now()
                self.msg.pose = self.secPoses
                self.pub.publish(self.msg)
                
        except socket.error:
            pass # timeout when socket.recv()
            
        except Exception as e:
            print 'Exception at socket.recv():', e
    
    #===================================================================
    def poseSecCallback(self, msg):
        '''
        Reads Pose_in_Section message and sends it through UDP.
        '''
        # Do not do it too fast
        if (rospy.Time.now() - self.time).to_sec() <= 0.5:
            return
            
        # Update time
        self.time = msg.header.stamp
        
        if self.ID is None:
            rospy.loginfo('ID initialized to: %s', msg.ID)
        
        # Send data
        self.ID = msg.ID
        data = self.serialize(msg)
        count = 0
        while count < len(data):
            count += self.sock_send.sendto(data, self.address)
    
    #===================================================================
    def serialize(self, msg):
        '''
        Serializes a Pose_in_Section message.
        '''
        fmt = "< I 2i 20s B 2f b"
        data = struct.pack(fmt, msg.header.seq, msg.header.stamp.secs, \
               msg.header.stamp.nsecs, msg.header.frame_id, msg.ID, msg.Gamma, \
               msg.Beta, msg.Flag)
               
        return data
    
    #===================================================================
    def deserialize(self, data):
        '''
        Deserializes a Pose_in_Section message.
        '''
        fmt = "< I 2i 20s B 2f b"
        data = struct.unpack(fmt, data)
        msg = Pose_in_Section()
        msg.header.seq         = data[0]
        msg.header.stamp.secs  = data[1]
        msg.header.stamp.nsecs = data[2]
        msg.header.frame_id    = data[3]
        msg.ID                 = data[4]
        msg.Gamma              = data[5]
        msg.Beta               = data[6]
        msg.Flag               = data[7]
        
        return msg

#=======================================================================
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_client')
    
    # Start node
    comm = FakeComm()
    while not rospy.is_shutdown():
        comm.iterate()
    
    # Closing
    comm.sock_send.close()
    comm.sock_recv.close()
