#!/usr/bin/env python

"""
Created on April 22 2013

@author: narcis palomeras
"""
import roslib 
roslib.load_manifest('g500_wrapper_morph')
import rospy
from std_msgs.msg import Float32
from morph_msgs.srv import Goto_Waypoint, Goto_WaypointRequest

if __name__ == '__main__':
    try:
        rospy.init_node('test_g500_wrapper')

       # Init Service Clients
        try:
            rospy.wait_for_service('/goto_waypoint', 20)
            goto_srv = rospy.ServiceProxy(
                                '/goto_waypoint', Goto_Waypoint)
        except rospy.exceptions.ROSException:
            rospy.logerr('Error creating client to goto waypoint.')
            rospy.signal_shutdown('Error creating disable_trajectory client')

        pub_heading = rospy.Publisher(
            "/heading_reference", Float32)
        pub_depth = rospy.Publisher(
            "/depth_reference", Float32)
        pub_altitude = rospy.Publisher(
            "/altitude_reference", Float32)
        pub_speed = rospy.Publisher(
            "/speed_reference", Float32)        

        req = Goto_WaypointRequest()
        req.pose.pose.position.x = 10
        req.pose.pose.position.y = 5
        req.pose.pose.position.z = 0
        print 'call goto Waypoint: \n', req
        print 'result: \n', goto_srv(req)
        
        rate = rospy.Rate(1)
        for i in range(10):
            print 'send data'
            pub_heading.publish(Float32(1.57))
            pub_depth.publish(Float32(3.0))
            pub_speed.publish(Float32(0.3))
            rate.sleep()

    except rospy.ROSInterruptException: pass
