#!/usr/bin/env python

"""
Created on April 22 2013
@author: narcis palomeras
"""

# ROS imports
import roslib 
roslib.load_manifest('g500_wrapper_morph')
import rospy
from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import WorldWaypointReq
from auv_msgs.msg import GoalDescriptor
from diagnostic_msgs.msg import DiagnosticArray
from std_msgs.msg import Float32, Int8
from nav_msgs.msg import Odometry
from cola2_lib import cola2_ros_lib
from morph_msgs.msg import Vehicle_Status
from morph_msgs.srv import Goto_Waypoint, Goto_WaypointResponse

# from morph_mission_handler.srv import TypeID, TypeIDResponse
# from morph_mission_handler.srv import Hold_Waypoint, Hold_WaypointResponse

from cola2_control.msg import WorldWaypointReqAction, WorldWaypointReqGoal
import actionlib
import math

class G500Wrapper(object):
    """ MORPH vehicle wrapper class for Girona 500 AUV """
    
    def __init__(self, name):
        """ Init class """
        # Init vars
        self.name = name
        self.body_velocity_req = BodyVelocityReq()
        self.world_waypoint_req = WorldWaypointReq()
        self.body_velocity_req.header.frame_id = 'girona500'
        self.world_waypoint_req.header.frame_id = 'world'
        self.body_velocity_req.goal.requester = self.name + '_speed'
        self.world_waypoint_req.goal.requester = self.name + '_pose'
        self.vehicle_id = 1
        self.body_velocity_req.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        self.world_waypoint_req.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        self.body_velocity_req.disable_axis.x = True
        self.body_velocity_req.disable_axis.y = True
        self.body_velocity_req.disable_axis.z = True
        self.body_velocity_req.disable_axis.roll = True
        self.body_velocity_req.disable_axis.pitch = True
        self.body_velocity_req.disable_axis.yaw = True
        self.world_waypoint_req.disable_axis.x = True
        self.world_waypoint_req.disable_axis.y = True
        self.world_waypoint_req.disable_axis.z = True
        self.world_waypoint_req.disable_axis.roll = True
        self.world_waypoint_req.disable_axis.pitch = True
        self.world_waypoint_req.disable_axis.yaw = True
        self.last_speed_command = rospy.Time.now()
        self.last_pose_command = rospy.Time().now()
        self.heading_reference = '/heading_reference'
        self.speed_reference = '/speed_reference'
        self.depth_reference = '/depth_reference'
        self.altitude_reference = '/altitude_reference'
        self.goto_waypoint_service = '/goto_waypoint'
        self.hold_waypoint_service = '/hold_waypoint'
        self.period = 0.1
        self.odometry = Odometry()
        
        self.getConfig()

        # Publisher    
        # self.pub_vehicle_id = rospy.Publisher(
        #     "/ID", Int8)

        self.pub_vehicle_status = rospy.Publisher(
            "/morph/g500/vehicle_status", Vehicle_Status)

        self.pub_velocity = rospy.Publisher(
            "/cola2_control/body_velocity_req", BodyVelocityReq)
            
        self.pub_pose = rospy.Publisher(
            "/cola2_control/world_waypoint_req", WorldWaypointReq)

                        
        # Subscriber
        rospy.Subscriber("/diagnostics_agg", 
                         DiagnosticArray, self.updateDiagnostics)

        rospy.Subscriber(self.heading_reference, 
                         Float32, self.updateHeading)
                         
        rospy.Subscriber(self.speed_reference, 
                         Float32, self.updateSpeed)

        rospy.Subscriber(self.depth_reference, 
                         Float32, self.updateDepth)
 
        rospy.Subscriber(self.altitude_reference, 
                         Float32, self.updateAltitude)
                         
        rospy.Subscriber('/pose_ekf_slam/odometry', 
                         Odometry, self.updateOdometry)
        
        # Action lib client
        self.client_absolute = actionlib.SimpleActionClient(
                                  'absolute_movement', 
                                   WorldWaypointReqAction)
        self.client_absolute.wait_for_server()
                
        # Create Services
        self.goto_waypoint_srv = rospy.Service(self.goto_waypoint_service, 
                                               Goto_Waypoint, 
                                               self.gotoWaypointService)
        
        # TODO: MISSION HANDLER NOT YET IN CATKIN
        # self.hold_waypoint_srv = rospy.Service(self.hold_waypoint_service, 
        #                                        Hold_Waypoint, 
        #                                        self.holdWaypointService)
        
        # self.set_id_srv = rospy.Service(self.set_id_service, 
        #                                 TypeID, 
        #                                 self.setIdService)                 
        
        # Timer
        rospy.Timer(rospy.Duration(self.period), self.iteration)

    
#    def setIdService(self, req):
#        self.vehicle_id = req.data.data
#        rospy.loginfo("%s: Set Id to %s", self.name, self.vehicle_id)
#        ret = TypeIDResponse()
#        ret.enabled.data = True
#        return ret
#    
    
    def updateOdometry(self, data):
        """ Save last odometry data. """
        self.odometry = data


    def holdWaypointService(self, req):
        """ Transform goto waypoint service into a pilot
            world way point request."""
            
        goal = WorldWaypointReqGoal()
        goal.goal.requester = self.name
        goal.goal.id = 1
        goal.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        goal.altitude_mode = False
        goal.position.north = req.pose.pose.position.x
        goal.position.east = req.pose.pose.position.y
        goal.position.depth = req.pose.pose.position.z
        goal.altitude = 5.0
        goal.orientation.roll = 0.0
        goal.orientation.pitch = 0.0
        goal.orientation.yaw = 0.0
        
        goal.mode = 'waypoint'
        goal.disable_axis.x = False
        goal.disable_axis.y = True
        goal.disable_axis.z = False
        goal.disable_axis.roll = True
        goal.disable_axis.pitch = True
        goal.disable_axis.yaw = False
        
        # Set tolerance
        goal.position_tolerance.x = 2.5
        goal.position_tolerance.y = 2.5
        goal.position_tolerance.z = 1.5
        goal.orientation_tolerance.roll = 3.14
        goal.orientation_tolerance.pitch = 3.14
        goal.orientation_tolerance.yaw = 3.14
        
        self.client_absolute.send_goal(goal)
        self.client_absolute.wait_for_result()
        
        # TODO: Enable keep pose
        print 'holding position ...'
        # Wait
        rospy.sleep(req.time.data)

        # TODO: Disable keep pose
        print 'hold position done!'
        
        # TODO: MISSION HANDLER NOT YET IN CATKIN
        # resp = Hold_WaypointResponse()
        # resp.pose.header.stamp = rospy.Time().now()
        # resp.pose.pose = self.odometry.pose.pose
        # return resp
                 
        
    def gotoWaypointService(self, req):
        """ Transform goto waypoint service into a pilot
            world way point request."""
            
        goal = WorldWaypointReqGoal()
        goal.goal.requester = self.name
        goal.goal.id = 1
        goal.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        goal.altitude_mode = False
        goal.position.north = req.pose.pose.position.x
        goal.position.east = req.pose.pose.position.y
        goal.position.depth = req.pose.pose.position.z
        goal.altitude = 5.0
        goal.orientation.roll = 0.0
        goal.orientation.pitch = 0.0
        goal.orientation.yaw = 0.0
        
        goal.mode = 'waypoint'
        goal.disable_axis.x = False
        goal.disable_axis.y = True
        goal.disable_axis.z = False
        goal.disable_axis.roll = True
        goal.disable_axis.pitch = True
        goal.disable_axis.yaw = False
        
        # Set tolerance
        goal.position_tolerance.x = 2.5
        goal.position_tolerance.y = 2.5
        goal.position_tolerance.z = 1.5
        goal.orientation_tolerance.roll = 3.14
        goal.orientation_tolerance.pitch = 3.14
        goal.orientation_tolerance.yaw = 3.14
        
        self.client_absolute.send_goal(goal)
        self.client_absolute.wait_for_result()
        resp = Goto_WaypointResponse()
        resp.pose.header.stamp = rospy.Time().now()
        resp.pose.pose = self.odometry.pose.pose
        return resp
                 
    
    def updateHeading(self, heading):
        """ Transform heading reference into world_waypoint_req """
        self.last_pose_command = rospy.Time().now()
        if heading.data < math.pi:
            self.world_waypoint_req.orientation.yaw = heading.data
        else:
            self.world_waypoint_req.orientation.yaw = heading.data - 2*math.pi
            
        self.world_waypoint_req.disable_axis.yaw = False
    

    def updateDepth(self, depth):
        """ Transform depth reference into world_waypoint_req """
        self.last_pose_command = rospy.Time().now()
        self.world_waypoint_req.position.depth = depth.data
        self.world_waypoint_req.altitude_mode = False
        self.world_waypoint_req.disable_axis.z = False
        #print 'Depth: ', depth.data
 

    def updateAltitude(self, altitude):
        """ Transform altitude reference into world_waypoint_req """
        self.last_pose_command = rospy.Time().now()
        self.world_waypoint_req.altitude = altitude.data
        self.world_waypoint_req.altitude_mode = True
        self.world_waypoint_req.disable_axis.z = False
    
    
    def updateSpeed(self, speed):
        """ Transform speed reference into body_velocity_req """
        self.last_speed_command = rospy.Time().now()
        self.body_velocity_req.twist.linear.x = speed.data
        self.body_velocity_req.disable_axis.x = False
        
    
    def updateDiagnostics(self, diag):
        """ Transform diagnostic message into vehicle_status """
        # TODO: Use names instead of absolut references!
        # --> imu status should be in vehicle_status, isn't it ? <--
        
        if len(diag.status) < 4:
            return False
        if len(diag.status[3].values) < 9:
            return False
            
        vehicle_status = Vehicle_Status()
        vehicle_status.battery_status =  int(float(diag.status[8].values[0].value))
        vehicle_status.dvl = diag.status[3].values[7].value == 'Ok'
        vehicle_status.gps = diag.status[3].values[5].value == 'True'        
        vehicle_status.depth_sensor = diag.status[3].values[9].value == 'Ok' 
        vehicle_status.altimeter = diag.status[3].values[1].value > 0.0 
        vehicle_status.multibeam = True
        vehicle_status.camera = True
        vehicle_status.thruster_x = True
        vehicle_status.thruster_y = True
        vehicle_status.thruster_z = True
        self.pub_vehicle_status.publish(vehicle_status)
    
        # TODO: Who is going to publish vehicle ID?
        # self.pub_vehicle_id.publish(Int8(self.vehicle_id))
        
    
    def iteration(self, event):
        """ Send control commads (pose & velocity) and check if the
            last reference has been received during the last 2 seconds."""
            
        if (rospy.Time().now() - self.last_speed_command).to_sec() > 2.0:
            self.body_velocity_req.disable_axis.x = True
 
        self.body_velocity_req.header.stamp = rospy.Time.now()
        self.pub_velocity.publish(self.body_velocity_req)
        
        if (rospy.Time().now() - self.last_pose_command).to_sec() > 2.0:
            self.world_waypoint_req.disable_axis.z = True
            self.world_waypoint_req.disable_axis.yaw = True
 
        self.world_waypoint_req.header.stamp = rospy.Time.now()
        self.pub_pose.publish(self.world_waypoint_req)
 
                
    def getConfig(self):
        """ Read config params from ROSPARAM server """
        
        param_dict = {'heading_reference': '/morph_wrapper/heading_reference_topic',
                      'speed_reference': '/morph_wrapper/speed_reference_topic',
                      'depth_reference': '/morph_wrapper/depth_reference_topic',
                      'altitude_reference': '/morph_wrapper/altitude_reference_topic',
                      'goto_waypoint_service': '/morph_wrapper/goto_waypoint_service',                      
                      'hold_waypoint_service': '/morph_wrapper/hold_waypoint_service',                      
                      'period': '/morph_wrapper/topics_publish_period'}
                      
        cola2_ros_lib.getRosParams(self, param_dict)
        
if __name__ == '__main__':
    try:
        rospy.init_node('g500_wrapper')
        G500_wrapper = G500Wrapper(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException: 
        pass
