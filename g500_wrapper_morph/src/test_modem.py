#!/usr/bin/env python

"""
Created on July 2013

@author: guillem vallicrosa
"""
import roslib 
roslib.load_manifest('g500_wrapper_morph')
import rospy
from std_msgs.msg import Float32
from morph_msgs.msg import AModem

if __name__ == '__main__':
	# Init
	rospy.init_node('test_modem')

    # Publisher
	pub = rospy.Publisher("/node1/driver/beta_to_transmit", AModem)
	
	# AModem
	msg = AModem()
	msg.ID = 1
	msg.time_of_flight = 1000
	msg.data = 'a'
	
	# Main loop
	rate = rospy.Rate(1)
	while not rospy.is_shutdown():
		print 'send data'
		msg.header.stamp = rospy.Time.now()
		pub.publish(msg)
		rate.sleep()
