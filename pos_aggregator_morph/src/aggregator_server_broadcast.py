#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading publishing a vehicle pose through a UDP
#   socket, which will be received by the aggregator_server and positions of all
#   simulated vehicles in different computers can be viewed together in the same
#   simulator.

# ROS basic
import roslib
roslib.load_manifest('pos_aggregator_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion

# UDP connection
import socket
import struct

#===============================================================================
class AggregatorServer(object):
    '''
    Class that reads UDP from specified ports and sends Odometry messages to
    the UWSim.
    '''
    
    #===========================================================================
    def __init__(self):
        '''
        Initializes the socket.
        '''
        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(0.05)
        self.sock.bind(('', 4444))
        
        # Publishers
        self.pubs = list()
        self.names = list()
        
        # Debug
        rospy.loginfo('Position Aggregator Server: initialized')
    
    #===========================================================================
    def iterate(self):
        '''
        Listens to UDP port and sends odometry data.
        '''
        try:
            # Read until timeout and publish
            msg, pub = self.readSocket()
            pub.publish(msg)
        except:
            pass
    
    #===========================================================================
    def readSocket(self):
        '''
        Reads Odometry data from specified socket.
        '''
        # Prepare data
        fmt = '> 10s 3d 4d'
        pos = Point()
        rot = Quaternion()
        packet = self.sock.recv(struct.calcsize(fmt))
        
        # Read
        data = struct.unpack(fmt, packet)
        name = data[0]
        pos.x = data[1]
        pos.y = data[2]
        pos.z = data[3]
        rot.x = data[4]
        rot.y = data[5]
        rot.z = data[6]
        rot.w = data[7]
        
        # Check if publisher exists
        if not name in self.names:
            self.names.append(name)
            pub = rospy.Publisher('/pos_agg/' + name, Odometry)
            self.pubs.append(pub)
        
        # Create message
        msg = Odometry()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "/world"
        msg.child_frame_id = name
        msg.pose.pose.position = pos
        msg.pose.pose.orientation = rot
        
        # Find publisher
        pub = self.pubs[self.names.index(name)]
        
        return msg, pub

#===============================================================================
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_server')
    
    # Start node
    server = AggregatorServer()
    while not rospy.is_shutdown():
        server.iterate()
