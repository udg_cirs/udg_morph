#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading publishing a vehicle pose through a UDP
#   socket, which will be received by the aggregator_server and positions of all
#   simulated vehicles in different computers can be viewed together in the same
#   simulator.

# ROS basic
import roslib
roslib.load_manifest('pos_aggregator_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion

# UDP connection
import socket
import struct

class AggregatorServer(object):
    '''
    Class that reads UDP from specified ports and sends Odometry messages to
    the UWSim.
    '''
    
    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        self.pub_names = rospy.get_param('aggregator_server/pub_names', list())
        ports = rospy.get_param('aggregator_server/ports', list())
        
        # Sockets
        self.sockets = list()
        for port in ports:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.settimeout(0.01)
            sock.bind(("", port))
            self.sockets.append(sock)
        
        # Publishers
        self.pubs = list()
        for name in self.pub_names:
            pub = rospy.Publisher(name, Odometry)
            self.pubs.append(pub)
        
        # Info
        print 'Initialization:'
        for i in range(len(ports)):
            print '   ', ports[i], ':', self.pub_names[i]
        
    def iterate(self):
        '''
        Listens to each UDP port and sends data to simulator.
        '''
        for i in range(len(self.sockets)):
            
            # Prepare and read
            sock = self.sockets[i]
            pub = self.pubs[i]
            try:
                # Read until timeout
                msg = self.readSocket(sock)
                
                # Finish message and publish
                msg.child_frame_id = self.pub_names[i]
                pub.publish(msg)
            except:
                pass
        
    def readSocket(self, sock):
        '''
        Reads Odometry data from specified socket.
        '''
        # Prepare data
        fmt = '> 3d 4d'
        pos = Point()
        rot = Quaternion()
        packet = sock.recv(struct.calcsize(fmt))
        
        # Read
        data = struct.unpack(fmt, packet)
        pos.x = data[0]
        pos.y = data[1]
        pos.z = data[2]
        rot.x = data[3]
        rot.y = data[4]
        rot.z = data[5]
        rot.w = data[6]
        
        # Create message
        msg = Odometry()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "/world"
        msg.pose.pose.position = pos
        msg.pose.pose.orientation = rot
        
        return msg		
        
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_server')
    
    # Start node
    server = AggregatorServer()
    while not rospy.is_shutdown():
        server.iterate()
