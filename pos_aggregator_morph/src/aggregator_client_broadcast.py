#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading publishing a vehicle pose through a UDP
#   socket, which will be received by the aggregator_server and positions of all
#   simulated vehicles in different computers can be viewed together in the same
#   simulator.

# ROS basic
import roslib
roslib.load_manifest('pos_aggregator_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry

# UDP connection
import socket
import struct

#===============================================================================
class AggregatorClient(object):
    '''
    Class that sends vehicle pose through UDP to specific port and address every
    time a pose is received from the vehicle architecture.
    '''
    
    #===========================================================================
    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        sub_name = rospy.get_param('aggregator_client/sub_name', '/pose_ekf_slam/odometry')
        self.address = ('<broadcast>', 4444)
        self.time = rospy.Time.now()
        
        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', 0))
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        # Subscriber
        self.sub = rospy.Subscriber(sub_name, Odometry, self.odom_callback)
        
        # Log
        rospy.loginfo('Position Aggregator Client: initialized')
        
    #===========================================================================
    def odom_callback(self, msg):
        '''
        Reads odometry from message and sends it through UDP.
        '''
        # Do not do it too fast
        if (msg.header.stamp - self.time).to_sec() <= 0.5:
            return
            
        # Update time
        self.time = msg.header.stamp
        
        # Get hostname
        name = socket.gethostname()
        if len(name) < 10:
            name = name + (10 - len(name)) * 'x'
        else:
            name = name[:10]
        
        # Delete bad chars
        name = name.replace('-', '_')
            
        # Prepare data
        fmt = '> 10s 3d 4d'
        pos = msg.pose.pose.position
        rot = msg.pose.pose.orientation
        data = struct.pack(fmt, name, pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, rot.w)

        # Send data
        count = 0
        while count < len(data):
            count += self.sock.sendto(data, self.address)
        
#===============================================================================
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_client')
    
    # Start node
    client = AggregatorClient()
    rospy.spin()
