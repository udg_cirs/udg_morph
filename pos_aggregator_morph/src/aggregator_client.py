#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading publishing a vehicle pose through a UDP
#   socket, which will be received by the aggregator_server and positions of all
#   simulated vehicles in different computers can be viewed together in the same
#   simulator.

# ROS basic
import roslib
roslib.load_manifest('pos_aggregator_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry

# UDP connection
import socket
import struct

#===============================================================================
class AggregatorClient(object):
    '''
    Class that sends vehicle pose through UDP to specific port and address every
    time a pose is received from the vehicle architecture.
    '''
    
    #===========================================================================
    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        sub_name = rospy.get_param('aggregator_client/sub_name', '/pose_ekf_slam/odometry')
        host = rospy.get_param('aggregator_client/address', '192.168.1.100')
        port = rospy.get_param('aggregator_client/port', 4444)
        self.address = (host, port)
        
        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        
        # Subscriber
        self.sub = rospy.Subscriber(sub_name, Odometry, self.sendOdom)
        
        # Info
        print 'Initialization:'
        print '   ', port, ':', sub_name
        print '   ', 'server :', host
        
    #===========================================================================
    def sendOdom(self, msg):
        '''
        Reads odometry from message and sends it through UDP.
        '''
        # Prepare data
        fmt = '> 3d 4d'
        pos = msg.pose.pose.position
        rot = msg.pose.pose.orientation
        data = struct.pack(fmt, pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, rot.w)
        
        # Send data
        count = 0
        while count < len(data):
            count += self.sock.sendto(data, self.address)
        
#===============================================================================
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_client')
    
    # Start node
    client = AggregatorClient()
    rospy.spin()
