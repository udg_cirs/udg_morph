#!/usr/bin/env python
#
# Creation:
#   Jun 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended for reading publishing a vehicle pose through a UDP
#   socket, which will be received by the aggregator_server and positions of all
#   simulated vehicles in different computers can be viewed together in the same
#   simulator.

# ROS basic
import roslib
roslib.load_manifest('pos_aggregator_morph')
import rospy

# ROS messsages
from nav_msgs.msg import Odometry
from auv_msgs.msg import NavSts

# UDP connection
import socket
import struct
import time
from math import pi

class PosVisual(object):
    '''
    Class that sends vehicle pose through UDP to specific port and address every
    time a pose is received from the vehicle architecture.
    '''
    
    def __init__(self):
        '''
        Loads the parameters needed and starts the subscribers.
        '''
        # Parameters
        name = rospy.get_param('pos_visual/name', 'G5H')
        port = rospy.get_param('pos_visual/port', 2804)
        self.address = ('<broadcast>', port)
        self.name = name
        
        # Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', 0))
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        # Subscriber
        self.sub = rospy.Subscriber('/cola2_navigation/nav_sts', NavSts, self.sendOdom)
        
        # Time
        self.last = rospy.Time.now().to_sec()
        
    def sendOdom(self, msg):
        '''
        Reads odometry from message and sends it through UDP.
        '''
        
        if rospy.Time.now().to_sec() - self.last < 1.0:
            return
        
        self.last = rospy.Time.now().to_sec()
        
        # Prepare data
        now = time.gmtime()
        
        # Fill message string
        data = "$PISE,%s,%04.8f,%04.8f,%04i%02i%02i,%02i%02i%02i,0,0000,%03i,0000,00.0*ff\n" % (self.name, msg.global_position.latitude, msg.global_position.longitude, now.tm_year, now.tm_mon, now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec, msg.orientation.yaw * 180. / pi)
        
        # Send data
        count = 0
        while count < len(data):
            count += self.sock.sendto(data, self.address)
        
        
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('pos_visual')
    
    # Start node
    client = PosVisual()
    rospy.spin()
